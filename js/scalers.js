'use strict';

//Constants
var BOX_WIDTH = 1;
var BOXES_TOTAL = 3;
var BOX_DENOM = 3.5;

//Global variables
var container, scene, camera, renderer, controls;
var origin;
var boxes;

//Global materials
var boxMat = new THREE.MeshLambertMaterial({color: 'blue',wireframe:false});
var boxGeo = new THREE.BoxGeometry(BOX_WIDTH, BOX_WIDTH, BOX_WIDTH);

init();
function init() {
		
	scene = new THREE.Scene;
		
	var SCREEN_WIDTH = window.innerWidth, SCREEN_HEIGHT = window.innerHeight;		
	var VIEW_ANGLE = 45, ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT, NEAR = 0.1, FAR = 200;	

	//Camera
	camera = new THREE.PerspectiveCamera( VIEW_ANGLE, ASPECT, NEAR, FAR);	
	camera.position.set(0,0, -10);
	scene.add(camera);	
	
	
	//Renderer
	renderer = new THREE.WebGLRenderer( {antialias:true} );		
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);		
	container = document.createElement( 'container' );
	document.body.appendChild( container );
	container.appendChild( renderer.domElement );
	
	controls = new THREE.OrbitControls( camera, renderer.domElement );		
	renderer.shadowMapEnabled = true;
	
	//Scene	
	
	//Lighting
	var light = new THREE.PointLight(0xffffff);
	light.position.set(0,10,0);
	scene.add(light);
	
	var hemi = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.3);
	hemi.position.set(0, 10, 0);
	scene.add(hemi);		
		
	//Origin
	var originGeo = new THREE.CylinderGeometry(0.1, 0.1, 0.1, 10, 1, false);
	var originMat = new THREE.MeshNormalMaterial();	
	origin = new THREE.Mesh(originGeo, originMat);
	scene.add(origin);
		
	//Boxes	
	//boxes = new THREE.Object3D();	
	//for (var i = 0; i < BOXES_TOTAL; i++) {				
	//	placeBox(i + 1);		
	//}
	//var t = BOXES_TOTAL + 1;
	//var series = (((t * t) - t)/2)/BOX_DENOM;
	//boxes.translateY(series);
	//scene.add(boxes);
	
	addBox(-1.5);
	//addBox(-1.4);
	//addBox(2);
	//setTimeout(addBox, 4000);
	render();

}

function addBox(initY) {
	var box = new THREE.Mesh(boxGeo, boxMat);	
	box.position.y = initY;
	box.scale.set(0, 0, 0);
	scene.add(box);
	var tween = new TWEEN.Tween({yPos:initY} )
		.to( { yPos: 5 }, 5000 - initY)
		.easing( TWEEN.Easing.Quintic.In )
		.onUpdate(function () {
			var size = Math.sqrt(((2 * this.yPos) + 3)/ 3);
			box.scale.set(size, size, size);
			box.position.y = -this.yPos;
		})
		.onComplete(function() {
			scene.remove(box);
			addBox();
		})
		.start();	
}

//function placeBox(boxNum) {
//	var boxSize = boxNum/BOX_DENOM;
//	var allBoxSizes = ((BOX_DENOM * boxSize * boxSize) - boxSize)/2;
//	var box = new THREE.Mesh(boxGeo, boxMat);	
//	box.position.set(0, (boxSize/2) - allBoxSizes - boxSize, 0);	
//	box.scale.set(boxSize, boxSize, boxSize);
//	boxes.add(box);
//}


function render(time) {	
	requestAnimationFrame( render );
	controls.update();
	TWEEN.update(time);		
	renderer.render( scene, camera );	
}
